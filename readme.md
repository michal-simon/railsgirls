# Rails Girls App Tutorial
Tutorial available on [Rails Girls Guides](http://guides.railsgirls.com/app/) portal.

## Variants:
* **Stock App** is in [master](../../branch/master) branch and contains old [Bootstrap 2](http://getbootstrap.com/2.3.2/) assets.
* **Bootstrap 3** is in [bootstrap-3](../../branch/bootstrap-3) branch and contains up to date stock app with [Bootstrap 3](http://getbootstrap.com) assets.
* **Advanced Design** is in [advanced-design](../../branch/advanced-design) branch and contains basic design changes in buttons, icons, tables and layout.

## Usage

 1. Clone or download current version (branch) of the app.
 2. Open terminal and move to the app folder with `cd your/app/folder`.
 3. Run `bundle install` command to download all external dependencies.
 4. Run `rake db:migrate` command to create app database structure.
 5. Run `rails server` to  start the rails server.
 6. Open the app URL shown in terminal (usually it's [localhost:3000](http://localhost:3000) ). 
 7. Done :)

If you have any questions contact me on Twitter at [@michalsimon](https://twitter.com/michalsimon).
